import React, { useState } from 'react';




function Table(props) {
  const initialTitles = ['#', 'Name', 'UserName', 'Email', 'Website', 'Phone']
  const [editName, setEditName] = useState({});
  const [editUserName, setEditUserName] = useState({});
  const [editEmail, setEditEmail] = useState({});
  const [editWebsite, setEditWEbsite] = useState({});
  const [editPhone, setEditPhone] = useState({});
  const [isActiveButtonTaskEdit, setIsActiveButtonTaskEdit] = useState(false);

  const editModeName = (el) => {
    setEditName(el);
  };
  const editModeUsername = (el) => {
    setEditUserName(el);
  };
  const ediModeEmail = (el) => {
    setEditEmail(el);
  };
  const editModeWebsite = (el) => {
    setEditWEbsite(el);
  };
  const editModePhone = (el) => {
    setEditPhone(el);
  };

  const onEditNameChange = (e) => {
    setEditName({ ...editName, name: e.target.value });
  };
  const onEditUserNameChange = (e) => {
    setEditUserName({ ...editUserName, username: e.target.value })
  };
  const onEditEmailChange = (e) => {
    setEditEmail({ ...editEmail, email: e.target.value });
  };
  const onEditWebsiteChange = (e) => {
    setEditWEbsite({ ...editWebsite, website: e.target.value });
  }
  const onEditPhoneChange = (e) => {
    setEditPhone({ ...editPhone, phone: e.target.value });
  };
  const saveInfo = () => {
    props.onSaveInfo(editName, editUserName, editEmail, editWebsite, editPhone);
    infoEditReset();
  };
  const infoEditReset = () => {
    setEditName({});
    setEditUserName({});
    setEditEmail({});
    setEditWEbsite({});
    setEditPhone({});
    setIsActiveButtonTaskEdit(false)
  };

  return (
    <div>
      <table className="table table-dark table-bordered">
        <thead>
          <tr>
            {initialTitles.map(el => <td key={el}>{el}</td>)}
          </tr>
        </thead>
        <tbody>
          
          {props.users.map((el,index,arr) =>
            <tr key={el.id}>
              <td>
              {arr.indexOf(el) + 1}
              </td>
              <td>
                {editName.id === el.id
                  ?
                  <div className="input-group">
                    <input type="text" value={editName.name} onChange={onEditNameChange} className="form-control" placeholder="First and Last name" aria-label="Firstname and Lastname" aria-describedby="basic-addon2" />
                    <div className="input-group-append">
                      <button onClick={saveInfo} disabled={editName.name.trim() === '' && !isActiveButtonTaskEdit} className="btn btn-outline-secondary" type="button">Save</button>
                      <button onClick={infoEditReset} className="btn btn-outline-secondary" type="button">Cancel</button>
                    </div>
                  </div>
                  : <span onDoubleClick={() => editModeName(el)}><td>{el.name}</td></span>
                }
              </td><td>
                {editUserName.id === el.id
                  ?
                  <>
                    <input type="text" value={editUserName.username} onChange={onEditUserNameChange} className="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon2" />
                    <button onClick={saveInfo} disabled={editUserName.username.trim() === '' && !isActiveButtonTaskEdit} className="btn btn-outline-secondary" type="button">Save</button>
                    <button onClick={infoEditReset} className="btn btn-outline-secondary" type="button">Cancel</button>
                  </>
                  : <span onDoubleClick={() => editModeUsername(el)}><td>{el.username}</td></span>
                }
              </td>
              <td>
                {editEmail.id === el.id
                  ?
                  <>
                    <input type="email" value={editEmail.email} onChange={onEditEmailChange} className="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon2" />
                    <button onClick={saveInfo} disabled={editEmail.email.trim() === '' && !isActiveButtonTaskEdit} className="btn btn-outline-secondary" type="button">Save</button>
                    <button onClick={infoEditReset} className="btn btn-outline-secondary" type="button">Cancel</button>
                  </>
                  : <span onDoubleClick={() => ediModeEmail(el)}><th scope="col">{el.email}</th></span>
                }
              </td>
              <td>
                {editWebsite.id === el.id
                  ?
                  <>
                    <input type="text" value={editWebsite.website} onChange={onEditWebsiteChange} className="form-control" placeholder="Website" aria-label="Website" aria-describedby="basic-addon2" />
                    <button onClick={saveInfo} disabled={editWebsite.website.trim() === '' && !isActiveButtonTaskEdit} className="btn btn-outline-secondary" type="button">Save</button>
                    <button onClick={infoEditReset} className="btn btn-outline-secondary" type="button">Cancel</button>
                  </>
                  : <span onDoubleClick={() => editModeWebsite(el)}><th scope="col" href={el.website}>{el.website}</th></span>
                }
              </td>
              <td>
                {editPhone.id === el.id
                  ?
                  <>
                    <input type="text" value={editPhone.phone} onChange={onEditPhoneChange} className="form-control" placeholder="Phone" aria-label="Phone" aria-describedby="basic-addon2" />
                    <button onClick={saveInfo} disabled={editPhone.phone.trim() === '' && !isActiveButtonTaskEdit} className="btn btn-outline-secondary" type="button">Save</button>
                    <button onClick={infoEditReset} className="btn btn-outline-secondary" type="button">Cancel</button>
                  </>
                  : <span onDoubleClick={() => editModePhone(el)}><th>{el.phone}</th></span>
                }
              </td>
            </tr>
          )}
        </tbody>
      </table>

    </div>
  );
}

export default Table;
