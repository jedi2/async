import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import Table from './Components/Table';
import { Jumbotron, Button, Spinner } from 'reactstrap';




function App() {
  const [users, setUsers] = useState([]);
  const [isOpenTable, setIsOpenTable] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isHiddenLoadMore, setHiddenLoadMore] = useState(true);


  const load = () => {
    console.log('Load');
    setIsLoading(true);
    // fetch('https://jsonplaceholder.typicode.com/users')
    //   .then(response => response.json())
    //   .then(json => {
    //     setUsers(json);
    //   }); first method
    axios({ // second method
      method: 'GET',
      url: 'https://cors-anywhere.herokuapp.com/https://jsonplaceholder.typicode.com/users'
    })
      .then((response) => {
        setUsers(response.data);
        setIsOpenTable(true);
        setIsLoading(false);
      });

  };
  const onSaveInfo = (editName, editUserName, editEmail, editWebsite, editPhone) => {

    const updatedInfo = users.map(el => {
      if (el.id === editName.id) return { ...el, name: editName.name }
      else if (el.id === editUserName.id) return { ...el, username: editUserName.username }
      else if (el.id === editEmail.id) return { ...el, email: editEmail.email }
      else if (el.id === editWebsite.id) return { ...el, website: editWebsite.website }
      else if (el.id === editPhone.id) return { ...el, phone: editPhone.phone }
      else return el
    })
    setUsers(updatedInfo);
  };

  window.onscroll = () => {
    if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
      setHiddenLoadMore(false);
      loadMore();
    }
  };

  const loadMore = () => {
    let id = users.length;
    fetch('https://jsonplaceholder.cypress.io/users')
      .then(response => response.json())
      .then(data => {
        data.map((el) => {
          id++;
          el.id = id;
          return el;
        });
        const newUsers = users.concat(data);
        setUsers(newUsers);
        setHiddenLoadMore(true);
      }).catch(error => console.log(error))
  }








  return (
    <div>
      <Jumbotron>

        <h1 className="display-3">Get in touch</h1>
        <p className="lead">Stop by and say hi. Or drop us a note</p>
        <hr className="my-2" />
        <p>Find your mentor!</p>
        <p className="lead">

          {
            !isOpenTable
              ?
              <Button color="primary" onClick={load}>  {isLoading ? <div variant="primary" disabled>
                <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
                Loading...
    </div> : 'Load Contacts'}</Button>
              : null
          }
          {isOpenTable &&
            <>
              <Table users={users} onSaveInfo={onSaveInfo} />
              <Button color="danger" onClick={() => setIsOpenTable(false)}>Clear</Button>
            </>
          }
        </p>
        <div className="text-center mb-4" hidden={isHiddenLoadMore}>
          <div className="spinner-border" role="status">
            <span className="sr-only" />
          </div>
        </div>
      </Jumbotron>
    </div>
  );
}

export default App;
